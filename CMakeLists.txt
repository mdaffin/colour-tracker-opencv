project( opencv )

find_package( OpenCV REQUIRED )

add_executable( opencv main.c )

target_link_libraries( opencv ${OpenCV_LIBS} )


