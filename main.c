/* Licence */
/* This code is based off the tutorial at [1] */
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <math.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

/*
 * Useful links
 * OpenCV tutorials:
 * [1] http://www.aishack.in/2010/07/tracking-colored-objects-in-opencv/
 * [2] http://www.pages.drexel.edu/~nk752/tutorials.html
 */

/* Window names */
#define WINDOW_CONTROLS  "Controls"
#define WINDOW_CAMERA    "Camera"
#define WINDOW_THRESHOLD "Threshold"

/* Detailed trackbar names */
#define TRACKBAR_HUE_LOWER        "Hue Lower"
#define TRACKBAR_HUE_UPPER        "Hue Upper"
#define TRACKBAR_HUE_RANGE        "Hue Range"
#define TRACKBAR_SATURATION_LOWER "Saturation Lower"
#define TRACKBAR_SATURATION_UPPER "Saturation Upper"
#define TRACKBAR_VALUE_LOWER      "Value Lower"
#define TRACKBAR_VALUE_UPPER      "Value Upper"
#define TRACKBAR_MIN_AREA         "Minimum Area"

/* Detailed trackbar varibles */
#define HUE_SLIDER_MAX        179
#define SATURATION_SLIDER_MAX 255
#define VALUE_SLIDER_MAX      255
#define MIN_AREA_SLIDER_MAX   500

int hue_slider_lower = HUE_SLIDER_MAX - 10;
int hue_slider_upper = HUE_SLIDER_MAX;
int hue_slider_range = 10;

int saturation_slider_lower = 50;
int saturation_slider_upper = SATURATION_SLIDER_MAX;

int value_slider_lower = 50;
int value_slider_upper = VALUE_SLIDER_MAX;

int min_area_slider = 250;


/* General varibles */
CvCapture* camera = 0;
IplImage * frame  = 0;
IplImage * threshedImage  = 0;
CvPoint position;

/* If true display simplified trackbars */
int simpleTrackbars = 1;

/* Function definitions */
IplImage* getThresholdImage( IplImage* image );
void on_trackbar_change( int position );
void initArgs( int argc, char **argv );
void initTrackbars();
void initWindows();
void initCamera();

/* Program starts here */
int main( int argc, char **argv ) {

    initArgs(argc, argv);
    initCamera();
    initWindows();
    initTrackbars();

    /* Main program loop - Exit if one of the windows is closed*/
    while ( cvGetWindowHandle( WINDOW_CAMERA ) &&
            cvGetWindowHandle( WINDOW_THRESHOLD ) &&
            cvGetWindowHandle( WINDOW_CONTROLS )) {
        /* Grab a frame */
        frame = cvQueryFrame( camera );

        /* Check we still have a frame */
        if ( !frame ) {
            fprintf( stderr, "Lost camera feed.\n" );
            return 2;
        }

        /* Smooth the image to get rid of noise */
        cvSmooth( frame, frame, CV_GAUSSIAN, 3, 0, 0, 0 );
        /* Threshold the image based on the slider values */
        threshedImage = getThresholdImage( frame );

        // Calculate the moments to estimate the position of the ball
        CvMoments *moments = (CvMoments*)malloc(sizeof(CvMoments));
        cvMoments(threshedImage, moments, 1);

        // The actual moment values
        double moment10 = cvGetSpatialMoment(moments, 1, 0);
        double moment01 = cvGetSpatialMoment(moments, 0, 1);
        double area = cvGetCentralMoment(moments, 0, 0);

        if ( area >= min_area_slider ) {
            CvPoint lastPosition = position;

            position.x = moment10/area;
            position.y = moment01/area;

            printf( "position: %d, %d\n", position.x, position.y );
            cvCircle( frame, position, 10,
                    cvScalar( 50,0,0,0 ),
                    2, 8, 0 );
            cvCircle( frame, position, 1,
                    cvScalar( 50,0,0,0 ),
                    2, 8, 0 );
        } else {
            position = cvPoint( -1, -1 );
        }

        fflush( stdout );

        /* Display the Images */
        cvShowImage( WINDOW_CAMERA, frame );
        cvShowImage( WINDOW_THRESHOLD, threshedImage );

        /* Delay 30ms */
        cvWaitKey( 30 );
    }

    /* Be tidy */
    cvDestroyWindow( WINDOW_CAMERA );
    cvDestroyWindow( WINDOW_THRESHOLD );
    cvDestroyWindow( WINDOW_CONTROLS );
    cvReleaseCapture( &camera );

    return 0;
}

/* Thresholds the image baised on the slider values in HSV colour space */
IplImage* getThresholdImage( IplImage* image ) { // [1]
    IplImage* hsvImage = cvCreateImage( cvGetSize(image), 8, 3 );
    cvCvtColor( image, hsvImage, CV_BGR2HSV );

    IplImage* threshedImage = cvCreateImage(cvGetSize(image), 8, 1);

    cvInRangeS(hsvImage,
               cvScalar(hue_slider_lower, saturation_slider_lower, value_slider_lower, 0),
               cvScalar(hue_slider_upper, saturation_slider_upper, value_slider_upper, 0),
               threshedImage);

    cvReleaseImage(&hsvImage);
    return threshedImage;
}

void initArgs( int argc, char **argv ) {
    int c;
    while (( c = getopt( argc, argv, "f" )) != -1 ) {
        switch (c) {
            case 'f':
                printf( "Enableing full trackbars\n" );
                simpleTrackbars = 0;
        }
    }
}

void on_hue_slider_range_change( int position )
{
    hue_slider_upper = hue_slider_lower + hue_slider_range;
}

/* Create the trackbars */
void initTrackbars() {
    /* Hue trackbars */
    if (!simpleTrackbars) {
        cvCreateTrackbar( TRACKBAR_HUE_LOWER, WINDOW_CONTROLS,
                        &hue_slider_lower, HUE_SLIDER_MAX, 0 );
        cvCreateTrackbar( TRACKBAR_HUE_UPPER, WINDOW_CONTROLS,
                        &hue_slider_upper, HUE_SLIDER_MAX, 0 );
    } else {
        cvCreateTrackbar( TRACKBAR_HUE_LOWER, WINDOW_CONTROLS,
                        &hue_slider_lower, HUE_SLIDER_MAX, on_hue_slider_range_change );
        cvCreateTrackbar( TRACKBAR_HUE_RANGE, WINDOW_CONTROLS,
                        &hue_slider_range, HUE_SLIDER_MAX, on_hue_slider_range_change );
    }

    /* Saturation trackbars */
    cvCreateTrackbar( TRACKBAR_SATURATION_LOWER, WINDOW_CONTROLS,
                      &saturation_slider_lower, SATURATION_SLIDER_MAX, 0 );
    if ( !simpleTrackbars ) {
        cvCreateTrackbar( TRACKBAR_SATURATION_UPPER, WINDOW_CONTROLS,
                        &saturation_slider_upper, SATURATION_SLIDER_MAX, 0 );
    }

    /* Value trackbars */
    cvCreateTrackbar( TRACKBAR_VALUE_LOWER, WINDOW_CONTROLS,
                      &value_slider_lower, VALUE_SLIDER_MAX, 0 );
    if ( !simpleTrackbars ) {
        cvCreateTrackbar( TRACKBAR_VALUE_UPPER, WINDOW_CONTROLS,
                        &value_slider_upper, VALUE_SLIDER_MAX, 0 );
    }

    cvCreateTrackbar( TRACKBAR_MIN_AREA, WINDOW_CONTROLS,
                      &min_area_slider, MIN_AREA_SLIDER_MAX, 0);
}

/* Create the windows */
void initWindows() {
    cvNamedWindow( WINDOW_CAMERA, CV_WINDOW_NORMAL );
    cvNamedWindow( WINDOW_THRESHOLD, CV_WINDOW_NORMAL );
    cvNamedWindow( WINDOW_CONTROLS, CV_WINDOW_NORMAL );
}

void initCamera() {
    /* Initlize the camera */
    camera = cvCaptureFromCAM( 0 );
    printf( "Initlized camera\n" );
    fflush( stdout ); // Flush standard out

    /* Set a decent width/height */
    int cam_width = (int) cvGetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH);
    int cam_height = (int) cvGetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT);
    if ( cam_width < 640 || cam_height < 480 ) {
        cvSetCaptureProperty( camera, CV_CAP_PROP_FRAME_WIDTH, 640 );
        cvSetCaptureProperty( camera, CV_CAP_PROP_FRAME_HEIGHT, 480 );
    }

    /* Check the camera is valid */
    if ( !camera ) {
        fprintf( stderr, "I am blind!!\n" );
        exit( 1 );
    }
}